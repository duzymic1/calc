import unittest
from calc import dodawanie
from calc import odejmowanie
from calc import mnozenie

class TestCalc(unittest.TestCase):
    def test_calc(self):
        self.assertEqual(dodawanie(1,2), 3)
        self.assertEqual(odejmowanie(2,1),1)
        self.assertEqual(mnozenie(2,2),4)

if __name__ == '__main__':
    unittest.main()
